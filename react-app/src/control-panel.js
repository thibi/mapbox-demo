import * as React from 'react';

function ControlPanel(props) {
  const {geoJsonLayerOn, vectorTileLayerOn, onChange} = props;

  return (
    <div className="control-panel">
      <h3>Multiple Layers</h3>
      <p>
        Map showing State/Region and Township boundaries of Myanmar
      </p>
      <p>
        Data source: <a href="https://geonode.themimu.info/">MIMU</a>
      </p>
      <hr />

      <div key={'layerSwitches'} className="input">
        <p>
          <label>Townships Layer</label>
          <input
            type="checkbox"
            checked={vectorTileLayerOn}
            onChange={evt => onChange('vectorTile',evt.target.checked)}
          />
        </p>
        <p>
          <label>State/Regions Layer</label>
          <input
            type="checkbox"
            checked={geoJsonLayerOn}
            onChange={evt => onChange('geoJson',evt.target.checked)}
          />
        </p>
      </div>
    </div>
  );
}

export default React.memo(ControlPanel);