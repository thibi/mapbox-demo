import './App.css';
import mapboxToken from './mapbox-token.json';
import * as React from 'react';
import {useState, useEffect, useMemo, useCallback} from 'react';
import {render} from 'react-dom';
import ReactMapGL, {Popup, Source, Layer} from 'react-map-gl';


import {townshipsLayer, dataLayer, highlightLayer} from './map-style.js';
import ControlPanel from './control-panel';

function App() {

  // Highlights and Tooltip on mouseover
  const MAPBOX_TOKEN = mapboxToken.token;

  const [vectorTileLayerOn, setVectorTileLayerOn] = useState(true);
  const [geoJsonLayerOn, setGeoJsonLayerOn] = useState(true);

  const [hoverInfo, setHoverInfo] = useState(null);

  const handleLayerChange = (layer,value) => {
    // console.log(layer,value)
    if (layer == 'vectorTile') setVectorTileLayerOn(value);
    if (layer == 'geoJson') setGeoJsonLayerOn(value);
  }

  const [viewport, setViewport] = useState({
    width: 1400,
    height: 600,
    latitude: 19.75,
    longitude: 96.09,
    zoom: 4,
    minZoom: 2,
    bearing: 0,
    pitch: 0
  });

  useEffect(() => {
    /* global fetch */
    fetch(
      // 'https://gitlab.com/thibi/mapbox-demo/-/raw/main/data/mm_state_regions/mm_state_regions_simplified.json'
      'https://gl.githack.com/thibi/mapbox-demo/-/raw/main/data/mm_state_regions/mm_state_regions_simplified.json'
    )
      .then(resp => resp.json())
      .then(json => setAllData(json));
  }, []);


  const [allData, setAllData] = useState(null);

  const onHover = useCallback(event => {
    const tsp = event.features && event.features[0];
    console.log(tsp)
    setHoverInfo({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
      townshipName: tsp && tsp.properties.TS,
      stateRegionName: tsp && tsp.properties.ST,
    });
  }, []);


  const selectedSR = (hoverInfo && hoverInfo.stateRegionName) || '';
  const selectedTsp = (hoverInfo && hoverInfo.townshipName) || '';
  const filter = useMemo(() => ['in', 'ST', selectedSR], [selectedSR]);

  return (
    <>
      <ReactMapGL
        {...viewport}
        mapStyle="mapbox://styles/mapbox/light-v9"
        mapboxApiAccessToken={MAPBOX_TOKEN}
        onViewportChange={setViewport}
        onHover={onHover}
        interactiveLayerIds={['townships']}
      >
        <Source type="vector" url="mapbox://yannaungoak.3b17q88j">
          {vectorTileLayerOn && <Layer beforeId="waterway-label" {...townshipsLayer} />}
          {vectorTileLayerOn && <Layer beforeId="waterway-label" {...highlightLayer} filter={filter} />}
        </Source>
        <Source type="geojson" data={allData}>
          {geoJsonLayerOn && <Layer {...dataLayer} />}
        </Source>
        {selectedSR && selectedTsp && (
          <Popup
            longitude={hoverInfo.longitude}
            latitude={hoverInfo.latitude}
            closeButton={false}
            className="county-info"
          >
            {selectedTsp} in {selectedSR}
          </Popup>
        )}
      </ReactMapGL>

      <ControlPanel 
        vectorTileLayerOn={vectorTileLayerOn} 
        geoJsonLayerOn={geoJsonLayerOn}
        onChange={handleLayerChange} 
      />
    </>
  );
}

export default App;
