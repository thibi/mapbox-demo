export const townshipsLayer = {
  id: 'townships',
  type: 'fill',
  'source-layer': 'mmr_polbnda_adm3_mimu_250k-d4qg2o',
  paint: {
    'fill-outline-color': 'rgba(0,0,0,0.1)',
    'fill-color': 'rgba(0,0,0,0.1)'
  }
};

export const dataLayer = {
  id: 'data',
  type: 'fill',
  paint: {
    'fill-color': '#3288bd',
    'fill-opacity': 0.8
  }
};

// Highlighted township polygons
export const highlightLayer = {
  id: 'townships-highlighted',
  type: 'fill',
  source: 'townships',
  'source-layer': 'mmr_polbnda_adm3_mimu_250k-d4qg2o',
  paint: {
    'fill-outline-color': '#484896',
    'fill-color': '#6e599f',
    'fill-opacity': 0.75
  }
};